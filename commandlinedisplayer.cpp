#include "commandlinedisplayer.h"


const QString CommandLineDisplayer::indent{"  "};

CommandLineDisplayer::CommandLineDisplayer(QTextStream *out, QObject *parent) :
    QObject(parent),
    _out(out)
{
}

void CommandLineDisplayer::onBatteryRead(const BatteryData &batteryData)
{
    *_out << tr("Battery status as of %1:").arg(dateTimeToString(batteryData.timestamp)) << endl;

    *_out << indent
          << tr("Capacity: %1/%2 (%3 %) %4 kWh").arg(_locale.toString(batteryData.remaining))
                                                .arg(_locale.toString(batteryData.capacity))
                                     .arg(_locale.toString(batteryData.stateOfCharge * 100, 'f', 1))
                                    .arg(_locale.toString(batteryData.remainingWh / 1000.0, 'f', 3))
          << endl;

    *_out << indent
          << tr("Cruising range: %1 (%2 with AC)")
                                              .arg(distanceToString(batteryData.cruisingRangeAcOff))
                                              .arg(distanceToString(batteryData.cruisingRangeAcOn))
          << endl;

    *_out << indent
          << tr("Plug-in state: %1").arg(PluginState::toUserString(batteryData.pluginState))
          << endl;

    *_out << indent
          << tr("Charging status: %1").arg(ChargingStatus::toUserString(batteryData.chargingStatus))
          << endl;

    *_out << indent << tr("Time to full:") << endl;

    if(batteryData.timeRequiredToFullL1 > 0)
    {
        *_out << indent << indent
              << tr("Level 1 charge (%1 kW): %2").arg(_locale.toString(1.4, 'f', 1))
                                            .arg(durationToString(batteryData.timeRequiredToFullL1))
              << endl;
    }

    if(batteryData.timeRequiredToFullL2Low > 0)
    {
        *_out << indent << indent
              << tr("Level 2 charge (%1 kW): %2").arg(_locale.toString(3.3, 'f', 1))
                                         .arg(durationToString(batteryData.timeRequiredToFullL2Low))
              << endl;
    }

    if(batteryData.timeRequiredToFullL2High > 0)
    {
        *_out << indent << indent
              << tr("Level 2 charge (%1 kW): %2").arg(_locale.toString(6.6, 'f', 1))
                                        .arg(durationToString(batteryData.timeRequiredToFullL2High))
              << endl;
    }

    if(batteryData.timeRequiredToFullL1 <= 0 &&
       batteryData.timeRequiredToFullL1 <= 0 &&
       batteryData.timeRequiredToFullL1 <= 0)
    {
        *_out << indent << indent << tr("(no time-to-full estimates available)") << endl;
    }
}

QString CommandLineDisplayer::dateTimeToString(const QDateTime &dateTime)
{
    return _locale.toString(dateTime.toLocalTime());
}

QString CommandLineDisplayer::distanceToString(int distanceMeters)
{
    return QString("%1 km").arg(_locale.toString(distanceMeters / 1000));
}

QString CommandLineDisplayer::durationToString(int durationMinutes)
{
    int hours = durationMinutes / 60;
    int minutes = durationMinutes % 60;

    return QString ("%1 h %2 min").arg(_locale.toString(hours)).arg(_locale.toString(minutes));
}
