QT -= gui

CONFIG += c++1z

DEFINES += QT_DEPRECATED_WARNINGS

include(CarwingsQtLib.pri)

SOURCES += main.cpp \
    commandlinedisplayer.cpp

HEADERS += \
    commandlinedisplayer.h
