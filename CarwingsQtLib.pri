QT *= network

HEADERS += \
    $$PWD/lib/region.h \
    $$PWD/lib/session.h \
    $$PWD/utils/enum.h \
    $$PWD/lib/commands/command.h \
    $$PWD/lib/commandsprocessor.h \
    $$PWD/lib/requestparam.h \
    $$PWD/lib/commands/abstractcommand.h \
    $$PWD/lib/commands/commandconnect.h \
    $$PWD/lib/commands/commandupdate.h \
    $$PWD/lib/commands/commandcharge.h \
    $$PWD/utils/qblowfish/qblowfish.h \
    $$PWD/utils/qblowfish/qblowfish_p.h \
    $$PWD/lib/commands/commandbattery.h \
    $$PWD/lib/commands/batterydata.h \
    $$PWD/lib/commands/chargingstatus.h \
    $$PWD/lib/commands/pluginstate.h

SOURCES += \
    $$PWD/lib/region.cpp \
    $$PWD/lib/commandsprocessor.cpp \
    $$PWD/lib/commands/command.cpp \
    $$PWD/lib/session.cpp \
    $$PWD/lib/commands/abstractcommand.cpp \
    $$PWD/lib/commands/commandconnect.cpp \
    $$PWD/lib/commands/commandupdate.cpp \
    $$PWD/lib/commands/commandcharge.cpp \
    $$PWD/utils/qblowfish/qblowfish.cpp \
    $$PWD/lib/commands/commandbattery.cpp \
    $$PWD/lib/commands/chargingstatus.cpp \
    $$PWD/lib/commands/pluginstate.cpp
