#include <QCoreApplication>

#include <QDebug>
#include <QCommandLineParser>
#include <QFile>
#include <QSettings>
#include <QTextStream>

#include "commandlinedisplayer.h"
#include "lib/session.h"
#include "lib/commandsprocessor.h"


using namespace CarwingsQt;

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    // Options description
    QCommandLineParser parser;
    QCommandLineOption optionHelp = parser.addHelpOption();

    QCommandLineOption optionUserName({"u", "username"},
                                      app.translate("CLI arguments", "You+Nissan login"),
                                      "login");
    parser.addOption(optionUserName);

    QCommandLineOption optionPassword({"p", "password"},
                                      app.translate("CLI arguments", "You+Nissan password"),
                                      "password");
    parser.addOption(optionPassword);

    QStringList regions;
    for(Region::Enum region : Region::getValues(false))
    {
        regions << Region::toUserReadableString(region);
    }
    QCommandLineOption optionRegion({"r", "region"},
                                    app.translate("CLI arguments", "Your region {%1}").arg(regions.join(", ")),
                                    "region");
    parser.addOption(optionRegion);

    QCommandLineOption optionCredentials({"f", "session-file"},
                                         app.translate("CLI arguments", "INI file containing your login, password and region"),
                                         "file");
    parser.addOption(optionCredentials);

    QCommandLineOption optionVerbose({"v", "verbose"},
                                     app.translate("CLI arguments", "Display internal execution details"));
    parser.addOption(optionVerbose);

    parser.addPositionalArgument("commands",
                                 app.translate("CLI arguments", "List of commands to process"),
                                 "{commands}");

    // Now do the parsing and treat parsed options
    QTextStream out(stdout);
    Session session;
    bool displayHelp = true;
    QList<Command::Enum> commandsToProcess;

    if(parser.parse(app.arguments()) && !parser.isSet(optionHelp))
    {
        QString credentialsFile = parser.value(optionCredentials);
        if(!credentialsFile.isEmpty())
        {
            if(QFile::exists(credentialsFile))
            {
                QSettings settings(credentialsFile, QSettings::IniFormat);
                session.setUserName(settings.value(optionUserName.names().at(1)).toString());
                session.setPassword(settings.value(optionPassword.names().at(1)).toString());
                session.setRegion(Region::fromString(settings.value(optionRegion.names().at(1)).toString()));
            }
            else
            {
                out << app.translate("CLI errors", "ERROR: file %1 not found").arg(credentialsFile) << endl;
                return 1;
            }
        }
        else
        {
            session.setUserName(parser.value(optionUserName));
            session.setPassword(parser.value(optionPassword));
            session.setRegion(Region::fromString(parser.value(optionRegion)));
        }

        if(session.getUserName().isEmpty())
        {
            out << app.translate("CLI errors", "ERROR: username must be provided") << endl << endl;
        }
        else if(session.getPassword().isEmpty())
        {
            out << app.translate("CLI errors", "ERROR: password must be provided") << endl << endl;
        }
        else if(session.getRegion() == Region::None)
        {
            out << app.translate("CLI errors", "ERROR: region must be provided") << endl << endl;
        }
        else
        {
            displayHelp = false;

            for(const QString &arg : parser.positionalArguments())
            {
                Command::Enum parsedCommand = Command::None;
                for(Command::Enum command : Command::getValues())
                {
                    if(arg == Command::argumentName(command))
                    {
                        parsedCommand = command;
                        break;
                    }
                }

                if(parsedCommand != Command::None)
                {
                    commandsToProcess.append(parsedCommand);
                }
                else
                {
                    out << app.translate("CLI errors", "ERROR: Command \"%1\" not recognized").arg(arg)
                        << endl << endl;
                    displayHelp = true;
                }
            }
        }
    }

    if(displayHelp || parser.isSet(optionHelp))
    {
        out << parser.helpText() << endl
            << app.translate("CLI arguments", "Commands:") << endl;

        for(Command::Enum command : Command::getValues())
        {
            if(command != Command::Connect) // Connect is implicit
            {
                QString commandArgName = Command::argumentName(command);
                out << "  " << commandArgName << QString(27 - commandArgName.size(), ' ');
                out << Command::toUserHelpDescription(command) << endl;
            }
        }

        return 0;
    }
    else
    {
        if(parser.isSet(optionVerbose))
        {
            session.setVerbose(&out);
        }

        CommandsProcessor processor(session);
        processor.addCommandToProcess(Command::Connect);
        for(Command::Enum commandToProcess : commandsToProcess)
        {
            processor.addCommandToProcess(commandToProcess);
        }

        CommandLineDisplayer displayer(&out);
        QObject::connect(&processor, &CommandsProcessor::batteryRead,
                         &displayer, &CommandLineDisplayer::onBatteryRead);

        QObject::connect(&processor, &CommandsProcessor::finished,
                         &app,       []() { qApp->exit(0); });

        processor.start();

        return app.exec();
    }
}
