#pragma once

#include <QObject>

#include <QLocale>
#include <QTextStream>

#include "lib/commands/batterydata.h"

class CommandLineDisplayer : public QObject
{
    Q_OBJECT

    public:
        explicit CommandLineDisplayer(QTextStream *out, QObject *parent = nullptr);

    public:
        void onBatteryRead(const BatteryData &batteryData);

    private:
        QString dateTimeToString(const QDateTime &dateTime);

        QString distanceToString(int distanceMeters);

        QString durationToString(int durationMinutes);

    private:
        static const QString indent;

    private:
        QTextStream *_out{nullptr};
        QLocale _locale;
};
