#pragma once

#include <typeinfo>

#include <QDebug>
#include <QMetaEnum>
#include <QObject>


class Enum
{
    public:
        inline static quint8 countSetFlags(quint32 flag)
        { quint8 count; for(count=0 ; flag ; count++) { flag &= flag - 1; } return count; }

        template <typename EnumClass, typename EnumEnum>
        inline static EnumEnum defaultValue()
        { return (EnumEnum)EnumClass::staticMetaObject.enumerator(0).value(0); }
};

#define DEBUG_ENUM(className) \
public: \
    inline friend QDebug operator<<(QDebug dbg, const className::Enum &value) \
    { return dbg << className::staticMetaObject.enumerator(0).valueToKey(value); }

#define PRINT_ENUM(className) \
public: \
    inline static QString toString(className::Enum value) \
    { return QString(className::staticMetaObject.enumerator(0).valueToKey(value)); }

#define LIST_ENUM(className) \
public: \
    inline static QList<className::Enum> getValues(bool includeNone = false) \
    { \
        QList<className::Enum> result; \
        QMetaEnum me = className::staticMetaObject.enumerator(0); \
        for(int i = 0 ; i<me.keyCount() ; i++) \
        { \
            className::Enum value = (className::Enum)me.value(i); \
            if(QString(me.valueToKey(value)).toLower() != "none" || includeNone) \
            { \
                result << (className::Enum)me.value(i); \
            } \
        } \
        return result; \
    }

#define FULLLIST_ENUM(className) \
public: \
    inline static QList<className::Enum> getAllValues() \
    { return getValues(true); }

#define PARSE_ENUM(className) \
public: \
    inline static className::Enum fromString(const QString &text, bool *ok = NULL) \
    { \
        QMetaEnum me = className::staticMetaObject.enumerator(0); \
        int value = me.keyToValue(text.toUtf8()); \
        if(value >= 0) \
        { \
            if(ok) \
            { \
                *ok = true; \
            } \
            return (className::Enum)value; \
        } \
        else \
        { \
            if(ok) \
            { \
                *ok = false; \
            } \
            return (className::Enum)me.value(0); \
        } \
    }

#define STREAM_ENUM(className) \
inline QDataStream &operator<<(QDataStream &out, className::Enum value) \
{ return out << className::toString(value); } \
\
inline QDataStream &operator>>(QDataStream &in, className::Enum &value) \
{ \
    QString stringValue; \
    in >> stringValue; \
    value = className::fromString(stringValue); \
    return in; \
}

#define FLAGS_TO_LIST(className) \
public: \
    inline static QList<className::Enum> toList(className::className##s flags) \
    { \
        QList<className::Enum> result; \
        foreach(className::Enum value, className::getAllValues()) \
        { \
            if(value != 0 && flags.testFlag(value)) \
            { \
                result << value; \
            } \
        } \
        return result; \
    }

#define LIST_TO_FLAGS(className) \
public: \
    inline static className::className##s fromList(const QList<className::Enum> &values) \
    { \
        className::className##s result; \
        foreach(const className::Enum &value, values) \
        { \
            result |= value; \
        } \
        return result; \
    }

#define GET_ALL_FLAGS(className) \
public: \
    inline static className::className##s getAllFlags() \
    { \
        return className::fromList(className::getValues()); \
    }
