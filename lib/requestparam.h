#pragma once

#include <QString>

namespace CarwingsQt
{
    typedef struct
    {
        QString key;
        QString value;
    } RequestParam;
}
