#pragma once

#include <QObject>

#include <QQueue>

#include "lib/commands/batterydata.h"
#include "lib/commands/command.h"
#include "lib/session.h"

namespace CarwingsQt
{
    class CommandsProcessor : public QObject
    {
        Q_OBJECT

        public:
            explicit CommandsProcessor(Session &session, QObject *parent = nullptr);

            void addCommandToProcess(Command::Enum command) { _commands.enqueue(command); }

            void start();

        signals:
            void finished();

            void batteryRead(const BatteryData &batteryData);

        private:
            void processNextCommand();

            void onCommandFinished(const QString &error);

        private:
            Session &_session;
            QQueue<Command::Enum> _commands;
    };
}
