#include "region.h"


namespace CarwingsQt
{

QString Region::code(Region::Enum region)
{
    switch(region)
    {
        case None:
            break;
        case USA:
            return "NNA";
        case Europe:
            return "NE";
        case Canada:
            return "NCI";
        case Australia:
            return "NMA";
        case Japan:
            return "NML";
    }

    return QString();
}

QString Region::toUserReadableString(Region::Enum region)
{
    switch(region)
    {
        case None:
            return tr("None");
        case USA:
            return tr("USA");
        case Europe:
            return tr("Europe");
        case Canada:
            return tr("Canada");
        case Australia:
            return tr("Australia");
        case Japan:
            return tr("Japan");
    }

    return QString();
}

} // namespace CarwingsQt
