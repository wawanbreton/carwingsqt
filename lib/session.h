#pragma once

#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QString>
#include <QTextStream>

#include "lib/commands/command.h"
#include "region.h"
#include "requestparam.h"

namespace CarwingsQt
{
    class Session : public QObject
    {
        Q_OBJECT

        public:
            explicit Session(QObject *parent = nullptr);

            void setUserName(const QString &userName) { _userName = userName; }

            const QString &getUserName() const { return _userName; }

            void setPassword(const QString &clearPassword) { _clearPassword = clearPassword; }

            const QString &getPassword() const { return _clearPassword; }

            void encryptPassword(const QString &basePrm);

            const QString &getEncryptedPassword() { return _encryptedPassword; }

            void setRegion(Region::Enum region) { _region = region; }

            Region::Enum getRegion() const { return _region; }

            void setSessionParams(const QString &sessionId, const QString &vin);

            void setVerbose(QTextStream *outStream);

            void logCommandStart(Command::Enum command);

            void sendRequest(const QString &endpoint,
                             const QList<RequestParam> &extraParams = QList<RequestParam>());

            void logMessage(const QString &message, bool verboseOnly);

        signals:
            void requestAnswerReceived(const QJsonObject &object, const QString &error);

        private:
            void onRequestFinished();

        private:
            QString _userName;
            QString _clearPassword;
            QString _encryptedPassword;
            Region::Enum _region{Region::None};
            QList<RequestParam> _defaultParams;
            QTextStream *_verbose{nullptr};
            QTextStream _out;
            QString _baseURL{"https://gdcportalgw.its-mo.com/api_v190426_NE/gdc"};
            QNetworkAccessManager *_networkManager{nullptr};
    };
}
