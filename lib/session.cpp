#include "session.h"

#include <QJsonDocument>
#include <QNetworkReply>
#include <QUrlQuery>

#include "utils/qblowfish/qblowfish.h"


namespace CarwingsQt
{

Session::Session(QObject *parent) :
    QObject(parent),
    _out(stdout),
    _networkManager(new QNetworkAccessManager(this))
{
}

void Session::encryptPassword(const QString &basePrm)
{
    QBlowfish blowfish(basePrm.toUtf8());
    blowfish.setPaddingEnabled(true);
    _encryptedPassword = QString::fromUtf8(blowfish.encrypted(_clearPassword.toUtf8()).toBase64());
}

void Session::setSessionParams(const QString &sessionId, const QString &vin)
{
    _defaultParams.append({"RegionCode", Region::code(_region)});
    _defaultParams.append({"VIN", vin});
    _defaultParams.append({"custom_sessionid", sessionId});
}

void Session::setVerbose(QTextStream *outStream)
{
    _verbose = outStream;
}

void Session::logCommandStart(Command::Enum command)
{
    if(_verbose)
    {
        const QString commandName = Command::toUserReadableString(command);

        constexpr int outputWidth = 40;
        (*_verbose) << QString(outputWidth, '#') << endl;

        int missingSpaces = outputWidth - 2 - commandName.size();
        int spacesLeft = missingSpaces / 2;
        int spacesRight = missingSpaces - spacesLeft;
        (*_verbose) << "#" << QString(spacesLeft, ' ') << commandName
                    << QString(spacesRight, ' ') << "#" << endl;
        (*_verbose) << QString(outputWidth, '#') << endl;
    }
    else
    {
        _out << Command::toUserLongDescription(command) << endl;
    }
}

void Session::sendRequest(const QString &endpoint, const QList<RequestParam> &extraParams)
{
    QUrlQuery postData;
    for(const RequestParam &param : _defaultParams + extraParams)
    {
        postData.addQueryItem(param.key, QUrl::toPercentEncoding(param.value));
    }

    QNetworkRequest request(QString("%1/%2").arg(_baseURL).arg(endpoint));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    if(_verbose)
    {
        (*_verbose) << "==> POST request " << request.url().toString() << " "
                    << postData.toString(QUrl::FullyDecoded) << endl;
    }

    QNetworkReply *reply = _networkManager->post(request,
                                                 postData.toString(QUrl::FullyEncoded).toUtf8());
    connect(reply, &QNetworkReply::finished, this, &Session::onRequestFinished);
}

void Session::logMessage(const QString &message, bool verboseOnly)
{
    if(verboseOnly)
    {
        if(_verbose)
        {
            (*_verbose) << message << endl;
        }
    }
    else
    {
        _out << message << endl;
    }
}

void Session::onRequestFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply *>(sender());
    if(reply)
    {
        reply->deleteLater();

        QJsonDocument jsonDoc;
        QJsonObject jsonObject;
        QString error;

        QByteArray replyData;
        if(reply->error() == QNetworkReply::NoError)
        {
            replyData = reply->readAll();

            if(_verbose)
            {
                (*_verbose) << "<== POST answer " << replyData << endl;
            }

            QJsonParseError parseError;
            jsonDoc = QJsonDocument::fromJson(replyData, &parseError);
            if(parseError.error != QJsonParseError::NoError)
            {
                logMessage("ERROR: Invalid received server data", false);
                error = parseError.errorString();
            }
            else
            {
                QJsonObject jsonObjectTmp = jsonDoc.object();
                int status = jsonObjectTmp["status"].toInt();
                if(status == 200)
                {
                    jsonObject = jsonObjectTmp;
                }
                else
                {
                    logMessage(QString("ERROR: Server returned error code %1").arg(status), false);
                    error = tr("Server returned error %1").arg(status);
                }
            }
        }
        else if(_verbose)
        {
            error = reply->errorString();
            (*_verbose) << "<== POST ERROR " << reply->errorString() << endl;
        }

        emit requestAnswerReceived(jsonObject, error);
    }
    else
    {
        qCritical() << "Sender is not a QNetworkReply";
    }
}

} // namespace CarwingsQt
