#include "commandsprocessor.h"

#include <QCoreApplication>

#include "lib/commands/commandbattery.h"
#include "lib/commands/commandcharge.h"
#include "lib/commands/commandconnect.h"
#include "lib/commands/commandupdate.h"


namespace CarwingsQt
{

CommandsProcessor::CommandsProcessor(Session &session, QObject *parent) :
    QObject(parent),
    _session(session)
{
}

void CommandsProcessor::start()
{
    processNextCommand();
}

void CommandsProcessor::processNextCommand()
{
    if(_commands.isEmpty())
    {
        emit finished();
    }
    else
    {
        Command::Enum nextCommand = _commands.dequeue();

        _session.logCommandStart(nextCommand);

        AbstractCommand *command = nullptr;

        switch(nextCommand)
        {
            case Command::None:
                qWarning() << "Unable to process command None";
                break;
            case Command::Connect:
                command = new CommandConnect(_session, this);
                break;
            case Command::Battery:
            {
                CommandBattery *commandBattery = new CommandBattery(_session, this);
                connect(commandBattery, &CommandBattery::batteryRead,
                        this,           &CommandsProcessor::batteryRead);
                command = commandBattery;
                break;
            }
            case Command::Update:
                command = new CommandUpdate(_session, this);
                break;
            case Command::Charge:
                command = new CommandCharge(_session, this);
                break;
        }

        if(command)
        {
            connect(command, &AbstractCommand::finished,
                    this,    &CommandsProcessor::onCommandFinished);
            connect(command, &AbstractCommand::finished,
                    command, &AbstractCommand::deleteLater);

            command->process();
        }
        else
        {
            processNextCommand();
        }
    }
}

void CommandsProcessor::onCommandFinished(const QString &error)
{
    if(error.isEmpty())
    {
        processNextCommand();
    }
    else
    {
        _session.logMessage(error, false);
        emit finished();
    }
}

} // namespace CarwingsQt
