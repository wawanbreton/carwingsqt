#include "chargingstatus.h"

#include <QCoreApplication>


ChargingStatus::Enum ChargingStatus::fromCarwingsString(const QString &string)
{
    Enum result = Invalid;

    if(string == "NOT_CHARGING")
    {
        result = NotCharging;
    }
    else if(string == "NORMAL_CHARGING")
    {
        result = ChargingNormal;
    }
    else if(string == "RAPIDLY_CHARGING")
    {
        result = ChargingQuick;
    }

    return result;
}

QString ChargingStatus::toUserString(ChargingStatus::Enum value)
{
    switch(value)
    {
        case Invalid:
            return QCoreApplication::translate("ChargingStatus", "invalid");
        case NotCharging:
            return QCoreApplication::translate("ChargingStatus", "not charging");
        case ChargingNormal:
            return QCoreApplication::translate("ChargingStatus", "charging");
        case ChargingQuick:
            return QCoreApplication::translate("ChargingStatus", "quick charging");
    }

    return QString();
}
