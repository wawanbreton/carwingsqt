#pragma once

#include "lib/commands/abstractcommand.h"

#include "lib/commands/batterydata.h"
#include "lib/requestparam.h"

namespace CarwingsQt
{
    class CommandBattery : public AbstractCommand
    {
        Q_OBJECT

        public:
            explicit CommandBattery(Session &session, QObject *parent = nullptr);

            virtual void process() override;

        signals:
            void batteryRead(const BatteryData &batteryData);

        private:
            void onAnswerReceived(const QJsonObject &jsonObject, const QString &errorDetails);

            static int parseDurationToFull(const QJsonObject &jsonDuration);
    };
} // namespace CarwingsQt
