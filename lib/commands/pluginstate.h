#pragma once

#include <QString>

namespace PluginState
{
    typedef enum
    {
        Invalid,         // Invalid state, when updating data from the vehicle fails.
        NotConnected,    // Not connected to a charger
        ConnectedNormal, // Connected to a Level 1 or 2 EVSE
        ConnectedQuick   // Connected to a high voltage DC quick charger (ChaDeMo)
    } Enum;

    Enum fromCarwingsString(const QString &string);

    QString toUserString(Enum value);
}
