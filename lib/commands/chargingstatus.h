#pragma once

#include <QString>
namespace ChargingStatus
{
    typedef enum
    {
        Invalid,
        NotCharging,    // Not charging
        ChargingNormal, // Normal charging from a Level 1 or 2 EVSE
        ChargingQuick   // Rapidly charging from a ChaDeMo DC quick charger
    } Enum;

    Enum fromCarwingsString(const QString &string);

    QString toUserString(Enum value);
}
