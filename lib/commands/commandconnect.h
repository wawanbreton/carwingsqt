#pragma once

#include "lib/commands/abstractcommand.h"
#include "lib/requestparam.h"

namespace CarwingsQt
{
    class CommandConnect : public AbstractCommand
    {
        Q_OBJECT

        public:
            explicit CommandConnect(Session &session, QObject *parent = nullptr);

            virtual void process() override;

        private:
            void onInitialAnswerReceived(const QJsonObject &jsonObject,
                                         const QString &errorDetails);

            QList<RequestParam> makeDefaultParams();

            void doLogin();

            void onLoginAnswerReceived(const QJsonObject &jsonObject, const QString &errorDetails);

            bool findVehicleInfo(const QJsonObject &object, QString &vin, QString &sessionId);

            bool findVehicleInfo(const QJsonValue &value, QString &vin, QString &sessionId);

            bool findVehicleInfo(const QJsonArray &array, QString &vin, QString &sessionId);
    };
} // namespace CarwingsQt
