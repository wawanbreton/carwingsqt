#include "commandupdate.h"

#include <QJsonObject>

#include "lib/commands/batterydata.h"
#include "lib/session.h"

namespace CarwingsQt
{

CommandUpdate::CommandUpdate(Session &session, QObject *parent) :
    AbstractCommand(session, parent)
{

}

void CommandUpdate::process()
{
    connect(&accessSession(), &Session::requestAnswerReceived,
            this,             &CommandUpdate::onAnswerReceived);

    accessSession().sendRequest("BatteryStatusCheckRequest.php");
}

void CommandUpdate::onAnswerReceived(const QJsonObject &jsonObject, const QString &errorDetails)
{
    disconnect(&accessSession(), &Session::requestAnswerReceived,
               this,             &CommandUpdate::onAnswerReceived);

    if(errorDetails.isEmpty())
    {
        _updateRequestKey = jsonObject["resultKey"].toString();

        connect(&accessSession(), &Session::requestAnswerReceived,
                this,             &CommandUpdate::onPollAnswerReceived);

        _timerCompletion.start();

        pollUpdateOver();
    }
    else
    {
        emit finished(errorDetails);
    }
}

void CommandUpdate::pollUpdateOver()
{
    accessSession().sendRequest("BatteryStatusCheckResultRequest.php",
                                {{"resultKey", _updateRequestKey}});
}

void CommandUpdate::onPollAnswerReceived(const QJsonObject &jsonObject, const QString &errorDetails)
{
    if(errorDetails.isEmpty())
    {
        if(jsonObject["responseFlag"].toString() == "1")
        {
            if(jsonObject["operationResult"].toString() == "START")
            {
                emit finished();
            }
            else
            {
                emit finished(tr("ERROR: failed to retrieve updated info from vehicle"));
            }
        }
        else if(_timerCompletion.elapsed() < completionTimeoutMs)
        {
            pollUpdateOver();
        }
        else
        {
            emit finished(tr("ERROR: timed out waiting for update"));
        }
    }
    else
    {
        emit finished(errorDetails);
    }
}

} // namespace CarwingsQt
