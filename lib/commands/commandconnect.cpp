#include "commandconnect.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "lib/session.h"

namespace CarwingsQt
{

CommandConnect::CommandConnect(Session &session, QObject *parent) :
    AbstractCommand(session, parent)
{

}

void CommandConnect::process()
{
    connect(&accessSession(), &Session::requestAnswerReceived,
            this,             &CommandConnect::onInitialAnswerReceived);

    accessSession().sendRequest("InitialApp_v2.php", makeDefaultParams());
}

void CommandConnect::onInitialAnswerReceived(const QJsonObject &jsonObject,
                                             const QString &errorDetails)
{
    disconnect(&accessSession(), &Session::requestAnswerReceived,
               this,             &CommandConnect::onInitialAnswerReceived);

    if(errorDetails.isEmpty())
    {
        accessSession().encryptPassword(jsonObject["baseprm"].toString());
        doLogin();
    }
    else
    {
        emit finished(errorDetails);
    }
}

QList<RequestParam> CommandConnect::makeDefaultParams()
{
    return {{"initial_app_str", "9s5rfKVuMrT03RtzajWNcA"}};
}

void CommandConnect::doLogin()
{
    QList<RequestParam> params = makeDefaultParams();
    params.append({"UserId", accessSession().getUserName()});
    params.append({"Password", accessSession().getEncryptedPassword()});
    params.append({"RegionCode", Region::code(accessSession().getRegion())});

    connect(&accessSession(), &Session::requestAnswerReceived,
            this,             &CommandConnect::onLoginAnswerReceived);

    accessSession().sendRequest("UserLoginRequest.php", params);
}

void CommandConnect::onLoginAnswerReceived(const QJsonObject &jsonObject,
                                           const QString &errorDetails)
{
    disconnect(&accessSession(), &Session::requestAnswerReceived,
               this,             &CommandConnect::onLoginAnswerReceived);

    if(errorDetails.isEmpty())
    {
        QString vin;
        QString sessionId;

        // API is quite a mess, so look for the VIN tag wherever it is in the tree...
        if(findVehicleInfo(jsonObject, vin, sessionId))
        {
            accessSession().setSessionParams(sessionId, vin);
            emit finished();
        }
        else
        {
            emit finished(tr("Unable to find VIN and session ID from login request result"));
        }
    }
    else
    {
        emit finished(errorDetails);
    }
}

bool CommandConnect::findVehicleInfo(const QJsonObject &object, QString &vin, QString &sessionId)
{
    auto iteratorSessionId = object.constFind("custom_sessionid");
    if(iteratorSessionId != object.constEnd() && iteratorSessionId.value().isString())
    {
        auto iteratorVin = object.constFind("vin");
        if(iteratorVin != object.constEnd() && iteratorVin.value().isString())
        {
            vin = iteratorVin.value().toString();
            sessionId = iteratorSessionId.value().toString();
            return true;
        }
    }

    for(auto iterator = object.constBegin() ; iterator != object.constEnd() ; ++iterator)
    {
        QJsonValue value = iterator.value();
        if(value.isObject())
        {
            if(findVehicleInfo(value.toObject(), vin, sessionId))
            {
                return true;
            }
        }
        else if(value.isArray())
        {
            if(findVehicleInfo(value.toArray(), vin, sessionId))
            {
                return true;
            }
        }
    }

    return false;
}

bool CommandConnect::findVehicleInfo(const QJsonValue &value, QString &vin, QString &sessionId)
{
    if(value.isObject())
    {
        if(findVehicleInfo(value.toObject(), vin, sessionId))
        {
            return true;
        }
    }
    else if(value.isArray())
    {
        if(findVehicleInfo(value.toArray(), vin, sessionId))
        {
            return true;
        }
    }

    return false;
}

bool CommandConnect::findVehicleInfo(const QJsonArray &array, QString &vin, QString &sessionId)
{
    for(auto subIterator = array.constBegin() ; subIterator != array.constEnd() ; ++subIterator)
    {
        if(findVehicleInfo(*subIterator, vin, sessionId))
        {
            return true;
        }
    }

    return false;
}

} // namespace CarwingsQt
