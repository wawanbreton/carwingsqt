#include "commandcharge.h"

#include "lib/commands/batterydata.h"
#include "lib/session.h"

namespace CarwingsQt
{

CommandCharge::CommandCharge(Session &session, QObject *parent) :
    AbstractCommand(session, parent)
{
}

void CommandCharge::process()
{
    connect(&accessSession(), &Session::requestAnswerReceived,
            this,             &CommandCharge::onAnswerReceived);

    accessSession().sendRequest("BatteryRemoteChargingRequest.php");
}

void CommandCharge::onAnswerReceived(const QJsonObject &jsonObject, const QString &errorDetails)
{
    if(errorDetails.isEmpty())
    {
        #warning to be tested with a plugged-in car :)
        emit finished();
    }
    else
    {
        emit finished(errorDetails);
    }
}

} // namespace CarwingsQt
