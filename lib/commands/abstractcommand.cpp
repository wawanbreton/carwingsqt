#include "abstractcommand.h"

#include <QDateTime>


namespace CarwingsQt
{

AbstractCommand::AbstractCommand(Session &session, QObject *parent) :
    QObject(parent),
    _session(session)
{
}

QDateTime AbstractCommand::parseDateTime(const QString &text)
{
    QDateTime dateTime = QDateTime::fromString(text, "yyyy/MM/dd HH:mm");
    dateTime.setTimeSpec(Qt::UTC);
    return dateTime;
}

} // namespace CarwingsQt
