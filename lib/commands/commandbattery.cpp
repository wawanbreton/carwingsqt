#include "commandbattery.h"

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include "lib/commands/batterydata.h"
#include "lib/session.h"

namespace CarwingsQt
{

CommandBattery::CommandBattery(Session &session, QObject *parent) :
    AbstractCommand(session, parent)
{

}

void CommandBattery::process()
{
    connect(&accessSession(), &Session::requestAnswerReceived,
            this,             &CommandBattery::onAnswerReceived);

    accessSession().sendRequest("BatteryStatusRecordsRequest.php");
}

void CommandBattery::onAnswerReceived(const QJsonObject &jsonObject, const QString &errorDetails)
{
    disconnect(&accessSession(), &Session::requestAnswerReceived,
               this,             &CommandBattery::onAnswerReceived);

    if(errorDetails.isEmpty())
    {
        QJsonObject jsonStatusRecord = jsonObject["BatteryStatusRecords"].toObject();
        QJsonObject jsonBatteryStatus = jsonStatusRecord["BatteryStatus"].toObject();

        BatteryData batteryData;
        batteryData.timestamp = parseDateTime(jsonStatusRecord["NotificationDateAndTime"].toString());

        batteryData.capacity = jsonBatteryStatus["BatteryCapacity"].toString().toInt();
        batteryData.remaining = jsonBatteryStatus["BatteryRemainingAmount"].toString().toInt();
        if(batteryData.capacity > 0 && batteryData.remaining > 0)
        {
            batteryData.stateOfCharge =
                    static_cast<qreal>(batteryData.remaining) / batteryData.capacity;
        }
        else
        {
            QJsonObject jsonSoc = jsonBatteryStatus["SOC"].toObject();
            batteryData.stateOfCharge = jsonSoc["Value"].toString().toInt();
        }

        batteryData.remainingWh = jsonBatteryStatus["BatteryRemainingAmountWH"].toString().toInt();

        QString pluginStateStr = jsonStatusRecord["PluginState"].toString();
        batteryData.pluginState = PluginState::fromCarwingsString(pluginStateStr);

        QString chargingStatusStr = jsonBatteryStatus["BatteryChargingStatus"].toString();
        batteryData.chargingStatus = ChargingStatus::fromCarwingsString(chargingStatusStr);

        batteryData.cruisingRangeAcOn = jsonStatusRecord["CruisingRangeAcOn"].toString().toInt();
        batteryData.cruisingRangeAcOff = jsonStatusRecord["CruisingRangeAcOff"].toString().toInt();

        batteryData.timeRequiredToFullL1 =
                parseDurationToFull(jsonStatusRecord["TimeRequiredToFull"].toObject());
        batteryData.timeRequiredToFullL2Low =
                parseDurationToFull(jsonStatusRecord["TimeRequiredToFull200"].toObject());
        batteryData.timeRequiredToFullL2High =
                parseDurationToFull(jsonStatusRecord["TimeRequiredToFull200_6kW"].toObject());

        emit batteryRead(batteryData);
        emit finished();
    }
    else
    {
        emit finished(errorDetails);
    }
}

int CommandBattery::parseDurationToFull(const QJsonObject &jsonDuration)
{
    return jsonDuration["MinutesRequiredToFull"].toString().toInt() +
           jsonDuration["HourRequiredToFull"].toString().toInt() * 60;
}

} // namespace CarwingsQt
