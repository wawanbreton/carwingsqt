#pragma once

#include <QDateTime>

#include "lib/commands/chargingstatus.h"
#include "lib/commands/pluginstate.h"

typedef struct
{
    QDateTime timestamp;                 // Absolute measurement timestamp
    int capacity;                        // Total battery capacity (unknown unit)
    int remaining;                       // Actual battery charge (unknown unit)
    int remainingWh;                     // Actual battery charge (Wh)
    qreal stateOfCharge;                 // Actual battery charge ratio
    ChargingStatus::Enum chargingStatus; // Charging status
    int cruisingRangeAcOn;               // Reachable range with AC ON (meters, I hope)
    int cruisingRangeAcOff;              // Reachable range with AC OFF (meters, I hope)
    PluginState::Enum pluginState;       // Plugin state
    int timeRequiredToFullL1;            // Time to full charge at 1.4kW L1 (120V 12A) charge (minutes)
    int timeRequiredToFullL2Low;         // Time to full charge at 3.3kW L2 (240V ~15A) charge (minutes)
    int timeRequiredToFullL2High;        // Time to full charge at 6.6kW L2 (240V ~30A) charge (minutes)
} BatteryData;
