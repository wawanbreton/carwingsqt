#pragma once

#include <QObject>

namespace CarwingsQt
{
    class Session;

    class AbstractCommand : public QObject
    {
        Q_OBJECT

        public:
            explicit AbstractCommand(Session &session, QObject *parent = nullptr);

            virtual void process() = 0;

        signals:
            void finished(const QString &details = QString());

        protected:
            Session &accessSession() { return _session; }

            static QDateTime parseDateTime(const QString &text);

        private:
            Session &_session;
    };
} // namespace CarwingsQt
