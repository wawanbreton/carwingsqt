#pragma once

#include "utils/enum.h"

namespace CarwingsQt
{
    class Command : public QObject
    {
        Q_OBJECT
        Q_ENUMS(Enum)

        public:
            typedef enum
            {
                None,
                Connect,
                Battery,
                Update,
                Charge,
            } Enum;

            static QString toUserReadableString(Enum command);

            static QString toUserLongDescription(Enum command);

            static QString toUserHelpDescription(Enum command);

            static QString argumentName(Enum command);

        LIST_ENUM(Command)
    };
}
