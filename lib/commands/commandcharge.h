#pragma once

#include "lib/commands/abstractcommand.h"

#include <QElapsedTimer>

#include "lib/requestparam.h"

namespace CarwingsQt
{
    class CommandCharge : public AbstractCommand
    {
        Q_OBJECT

        public:
            explicit CommandCharge(Session &session, QObject *parent = nullptr);

            virtual void process() override;

        private:
            void onAnswerReceived(const QJsonObject &jsonObject, const QString &errorDetails);
    };
} // namespace CarwingsQt
