#include "command.h"

namespace CarwingsQt
{

QString Command::toUserReadableString(CarwingsQt::Command::Enum command)
{
    switch(command)
    {
        case None:
            break;
        case Connect:
            return tr("Connection");
        case Battery:
            return tr("Read battery status");
        case Update:
            return tr("Update battery status");
        case Charge:
            return tr("Start charge");
    }

    return QString();
}

QString Command::toUserLongDescription(Command::Enum command)
{
    switch(command)
    {
        case None:
            break;
        case Connect:
            return tr("Logging into Carwings...");
        case Battery:
            return tr("Getting latest retrieved battery status...");
        case Update:
            return tr("Requesting update from Carwings...");
        case Charge:
            return tr("Sending charging request...");
    }

    return QString();
}

QString Command::toUserHelpDescription(Command::Enum command)
{
    switch(command)
    {
        case None:
        case Connect: // Connect is not callable directly
            break;
        case Battery:
            return tr("Get most recently loaded battery status");
        case Update:
            return tr("Load latest battery data from vehicle");
        case Charge:
            return tr("Begin charging plugged-in vehicle");
    }

    return QString();
}

QString Command::argumentName(Command::Enum command)
{
    switch(command)
    {
        case None:
            break;
        case Connect:
            return "connect";
        case Battery:
            return "battery";
        case Update:
            return "update";
        case Charge:
            return "charge";
    }

    return QString();
}

} // namespace CarwingsQt
