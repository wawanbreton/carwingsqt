#pragma once

#include "lib/commands/abstractcommand.h"

#include <QElapsedTimer>

#include "lib/requestparam.h"

namespace CarwingsQt
{
    class CommandUpdate : public AbstractCommand
    {
        Q_OBJECT

        public:
            explicit CommandUpdate(Session &session, QObject *parent = nullptr);

            virtual void process() override;

        private:
            void onAnswerReceived(const QJsonObject &jsonObject, const QString &errorDetails);

            void pollUpdateOver();

            void onPollAnswerReceived(const QJsonObject &jsonObject, const QString &errorDetails);

        private:
            static constexpr int completionTimeoutMs{60000};

        private:
            QString _updateRequestKey;
            QElapsedTimer _timerCompletion;
    };
} // namespace CarwingsQt
