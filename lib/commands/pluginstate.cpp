#include "pluginstate.h"

#include <QCoreApplication>


PluginState::Enum PluginState::fromCarwingsString(const QString &string)
{
    Enum result = Invalid;

    if(string == "NOT_CONNECTED")
    {
        result = NotConnected;
    }
    else if(string == "CONNECTED")
    {
        result = ConnectedNormal;
    }
    else if(string == "QC_CONNECTED")
    {
        result = ConnectedQuick;
    }

    return result;
}

QString PluginState::toUserString(PluginState::Enum value)
{
    switch(value)
    {
        case Invalid:
            return QCoreApplication::translate("PluginState", "invalid");
        case NotConnected:
            return QCoreApplication::translate("PluginState", "not connected");
        case ConnectedNormal:
            return QCoreApplication::translate("PluginState", "connected");
        case ConnectedQuick:
            return QCoreApplication::translate("PluginState", "connected to quick charger");
    }

    return QString();
}
