#pragma once

#include <QString>

#include "utils/enum.h"

namespace CarwingsQt
{
    class Region : public QObject
    {
        Q_OBJECT
        Q_ENUMS(Enum)

        public:
            typedef enum
            {
                None,
                USA,
                Europe,
                Canada,
                Australia,
                Japan
            } Enum;

            static QString code(Enum region);

            static QString toUserReadableString(Enum region);

        LIST_ENUM(Region)
        PRINT_ENUM(Region)
        PARSE_ENUM(Region)
        DEBUG_ENUM(Region)
    };
}
